package com.example.thedarkemenu;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class Controller {
    @FXML
    private FlowPane paneid;

    static private Stage stage1;
    static private Stage stage2;

    public void showExitMenu() throws IOException {
        stage1 = (Stage) paneid.getScene().getWindow();
        stage2 = new Stage();
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("exit.fxml")));
        stage2.setScene(new Scene(root));
        stage2.setResizable(false);
        stage2.show();
    }

    public void closeProgram() {
        stage2.close();
        stage1.close();
    }

    public void closeExitMenu() {
        stage2.close();
    }
}