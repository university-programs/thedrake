package thedrake;

import java.io.PrintWriter;

public enum TroopFace implements JSONSerializable{
    AVERS, REVERS;

    public void toJSON(PrintWriter writer) {
        writer.print("\"" + this + "\"");
    }
}
