
module com.example.thedarkemenu {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;

    opens com.example.thedarkemenu to javafx.fxml;
    exports com.example.thedarkemenu;
}